/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import App from './App.vue';
import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import CreateComponent from './components/CreateComponent';
import EditComponent from './components/EditComponent';
import UserhomeComponent from './components/UserhomeComponent';
import ViewComponent from './components/ViewComponent';

const routes = [
    {
        name: "create",
        path: "/create",
        component:CreateComponent
    },
    {
        name: "edit",
        path: "/edit/:id",
        component:EditComponent
    },
    {
        name: "userhome",
        path: "/",
        component:UserhomeComponent
    },
    {
        name: "view",
        path: "/view",
        component:ViewComponent
    }
]


const router = new VueRouter({ mode: 'history', routes: routes });
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');


