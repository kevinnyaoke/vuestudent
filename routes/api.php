<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('students/create','ApiController@createStudent');
Route::get('students/view','ApiController@getAllStudents');
Route::get('students/{id}','ApiController@getStudent');
Route::put('students/update/{id}','ApiController@updateStudent');
Route::delete('students/delete/{id}','ApiController@deleteStudent');