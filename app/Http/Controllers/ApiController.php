<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;

class ApiController extends Controller
{
    public function getAllStudents(){

        $student=Students::all();
        return $student;

    }

    public function createStudent(Request $request){

        $student= new Students;

        $student-> name= $request-> name;
        $student-> reg= $request-> reg;
        $student-> stclass= $request-> stclass;
        $student-> idno= $request-> idno;
        $student->save();

        return response()->json([
            "message"=>"Student added successfully!",
        ], 201);

    }

    public function getStudent($id){

        if(Students::where('id',$id)->exists()){
            $student=Students::where('id',$id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($student, 200);
        } else{
            return response()->json([
                "message"=>"No such student!"
            ], 404);
        }
    }


    public function updateStudent(Request $request, $id){

        if(Students::where('id',$id)->exists()){
            $student=Students::find($id);

            $student->name=is_null($request->name) ? $student->name : $request->name;
            $student->reg=is_null($request->reg) ? $student->reg : $request->reg;
            $student->stclass=is_null($request->stclass) ? $student->stclass : $request->stclass;
            $student->idno=is_null($request->idno) ? $student->idno : $request->idno;
            $student->save();

            return response()->json([
                "message"=>"Student updated successfully!"
            ], 200);


        } else{
            return response()->json([
                "message"=>"No such student!"
            ], 404);
        }

    }


    public function deleteStudent($id){

        if(Students::where('id',$id)->exists()){
            $student=Students::find($id);
            $student->delete();

            return response()->json([
                "message"=>"Student deleted successfully"
            ], 202);
        }else {
            return response()->json([
                "message"=>"no such students"
            ], 404);
        }
        
    }

}
